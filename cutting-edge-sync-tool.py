#!/usr/bin/env python3

import os
import sys
import subprocess
import dbus
from PyQt5.QtWidgets import QApplication, QBoxLayout, QWidget
from PyQt5.QtWidgets import QPushButton, QLabel, QCheckBox
from PyQt5.Qt import QTimer
# from PyQt5.Qt import QLineEdit, QIntValidator


class SyncState:
    dir = '/home/pi/cutting-edge-dropbox'
    ival_min = 5
    usage_string = ''
    is_installed = False    # Whether the script and dependencies are installed
    is_enabled = False      # Whether the systemd units are loaded
    is_active = False       # Whether the systemd timer unit is active

    __unit_name__ = 'sync-cutting-edge-dropbox'
    __timer_name__ = 'sync-cutting-edge-dropbox.timer'
    __service_name__ = 'sync-cutting-edge-dropbox.service'

    def __init__(self):
        '''const'''
        self.usage_string = self.__get_usage_string__()
        self.ival_min = self.__get_ival_min__()
        self.__update_enabled_state__()

    def __get_usage_string__(self):
        if not os.path.isdir(self.dir):
            return '0 Mb'
        return (subprocess.check_output(['du', '-sh', self.dir]).split()[0]
                .decode('utf-8'))

    def __update_enabled_state__(self):
        '''
        Sets the class's installed, enabled, and active state
        '''

        self.is_installed = False
        self.is_enabled = False
        self.is_active = False

        # Check whether rclone has our config and whether our directory exists
        dir_exists = os.path.isdir(self.dir)
        dir_exists = True
        rclone_cfg_exists = False
        if dir_exists:
            try:
                cmd = 'rclone config show'
                rclone_cfg = subprocess.run(cmd.split(' '),
                                            capture_output=True, text=True)
                rclone_cfg_exists = ('[cutting-edge-dropbox]' in
                                     rclone_cfg.stdout)
            except Exception:
                rclone_cfg_exists = False

        self.is_installed = dir_exists and rclone_cfg_exists

        # Continue examining setup even if not fully installed
        try:
            mgr = self.__get_manager__()
            # following two fail if not loaded
            mgr.GetUnit(self.__timer_name__)
            mgr.GetUnit(self.__service_name__)
            self.is_enabled = True

            units = mgr.ListUnits()
            timer = self.__get_unit_from_list__(self.__timer_name__, units)
            self.is_active = timer[3] == 'active'
        except dbus.exceptions.DBusException:
            ''''''

    def __get_ival_min__(self):
        ''''''
        return 5

    def __get_manager__(self):
        sysbus = dbus.SystemBus()
        systemd1 = sysbus.get_object('org.freedesktop.systemd1',
                                     '/org/freedesktop/systemd1')
        return dbus.Interface(systemd1, 'org.freedesktop.systemd1.Manager')

    def __get_unit_from_list__(self, unit, units):
        out = [u for u in units if self.__timer_name__ in u[0]]
        return out[0]

    def set_enabled(self, enable):
        try:
            if enable:
                mgr = self.__get_manager__()
                mgr.ReloadOrRestartUnit(self.__timer_name__, "replace")
            else:
                mgr = self.__get_manager__()
                mgr.StopUnit(self.__timer_name__, "replace")
        except dbus.exceptions.DBusException:
            print('Could not authenticate')
        self.__update_enabled_state__()

    def get_state_text(self):
        if not self.is_installed:
            return 'The sync tool is not installed. Run install.sh'\
                ' or contact an administrator.'
        elif self.is_active:
            template = 'Syncing to {} every {} min, currently using {}.'
            return template.format(self.dir, self.ival_min, self.usage_string)
        elif self.is_enabled:
            template = 'Sync disabled. Sync storage directory {} uses {}.'
            return template.format(self.dir, self.usage_string)
        else:
            return 'The sync tool is installed but the systemd unit files are'\
                ' not loaded. Run install.sh or contact an administrator.'


class Window(QWidget):
    __refresh_btn__ = None
    __toggle_sync_chkbx__ = None
    __state_label__ = None
    __timer__ = None

    def __init__(self):
        super().__init__()
        self.__layout__()
        self.__update__()
        # update window every 5sec
        self.__timer__ = QTimer()
        self.__timer__.timeout.connect(self.__update__)
        self.__timer__.start(5000)

    def __layout__(self):
        self.setWindowTitle('Cutting Edge Sync Config Tool')

        root_layout = QBoxLayout(QBoxLayout.Direction(2), parent=self)

        row_0_wdgt = QWidget()
        row_0 = QBoxLayout(QBoxLayout.Direction(0), parent=row_0_wdgt)
        refresh_label = QLabel('Update Dropbox login details:')
        self.__refresh_btn__ = QPushButton('Reauthenticate')
        self.__refresh_btn__.clicked.connect(self.refresh_btn_handler)
        row_0.insertWidget(0, refresh_label)
        row_0.insertWidget(1, self.__refresh_btn__)

        row_1_wdgt = QWidget()
        row_1 = QBoxLayout(QBoxLayout.Direction(0), parent=row_1_wdgt)
        self.__toggle_sync_chkbx__ = QCheckBox('Sync enabled')
        self.__toggle_sync_chkbx__.clicked.connect(self.toggle_sync_handler)
        row_1.insertWidget(0, self.__toggle_sync_chkbx__)

        row_2_wdgt = QWidget()
        row_2 = QBoxLayout(QBoxLayout.Direction(0), parent=row_2_wdgt)
        self.__state_label__ = QLabel()
        row_2.insertWidget(0, self.__state_label__)

        root_layout.insertWidget(0, row_0_wdgt)
        root_layout.insertWidget(1, row_1_wdgt)
        root_layout.insertWidget(2, row_2_wdgt)

    def __update__(self, state=None):
        state = SyncState() if state is None else state
        self.__toggle_sync_chkbx__.setChecked(state.is_active)
        self.__state_label__.setText(state.get_state_text())
        # TODO: set checkbox and btn to disabled if not installed
        self.__toggle_sync_chkbx__.setEnabled((state.is_installed and
                                               state.is_enabled))
        self.__refresh_btn__.setEnabled(state.is_installed)

    def refresh_btn_handler(self):
        '''calls out to rclone to reauthenticate the dropbox connection'''
        cmd = 'rclone config reconnect cutting-edge-dropbox: --auto-confirm'
        try:
            subprocess.run(cmd.split(' '), timeout=60)
        except subprocess.TimeoutExpired:
            print('Timeout expired waiting for OAuth process to complete.')

    def toggle_sync_handler(self, checked):
        state = SyncState()
        state.set_enabled(checked > 0)
        self.__update__(state)


app = QApplication(sys.argv)
root = Window()
root.show()
sys.exit(app.exec_())
