#!/usr/bin/env bash
# set -euo pipefail

# uninstall script for Cutting Edge sync configuration
echo "
This script will uninstall the Cutting Edge sync configuration. It will leave a few things installed: evince (document viewer) and rclone (sync program possibly used elsewhere). It will NOT delete any synced data in ~/cutting-edge-dropbox.
"
while true; do
    read -p "Do you want to continue? (y/n)" yn
case $yn in
        [Yy]* ) echo "Starting uninstall..."; break;;
        [Nn]* ) echo "Uninstall cancelled."; exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo "Removing rclone-browser..."
ail-cli unintegrate ~/Applications/rclone-browser*

echo "Remove rclone configuration..."
rclone config delete cutting-edge-dropbox

echo "Removing appimagelauncher..."
sudo dpkg -r appimagelauncher

echo "Disabling and removing sync services..."
sudo systemctl stop sync-cutting-edge-dropbox.timer
sudo systemctl disable sync-cutting-edge-dropbox.timer
sudo systemctl disable sync-cutting-edge-dropbox.service
sudo rm /lib/systemd/system/sync-cutting-edge-dropbox.timer 
sudo rm /lib/systemd/system/sync-cutting-edge-dropbox.service 

echo "Removing cutting edge sync tool..."
rm ~/.local/share/applications/cutting-edge-sync-tool.desktop
rm ~/.local/bin/cutting-edge-sync-tool.py

echo "Uninstall complete."
