#!/usr/bin/env bash
set -euo pipefail

# script to automate installation & configuration of Rclone and RcloneBrowser
# (we're using the AppImage because the apt source is criminally out of date)

echo "
Starting Cutting Edge sync configuration for dropbox (using rclone & rclone-browser)
"

# install rclone

if ! command -v rclone &> /dev/null
then
    echo "rclone not found, installing..."
    curl https://rclone.org/install.sh | sudo bash
else
    echo "rclone found, moving on..."
fi

# configure dropbox rclone connection
# user prompted for password via OAuth flow here

rclone config create cutting-edge-dropbox dropbox --auto-confirm

# create and start service to mount rclone remote on login
function enable_mount_service() {
    # create mount point
    mkdir -p ~/cutting-edge-dropbox

    # create unit file
    sudo tee /lib/systemd/system/mount-cutting-edge-dropbox.service >> /dev/null <<EOL
    [Unit]
    Description=Cutting Edge Dropbox Network Mount tool
    Requires=network-online.target

    [Service]
    Type=simple
    ExecStart=sh -c 'rclone mount cutting-edge-dropbox: ~/cutting-edge-dropbox'
    User=pi
    Group=pi

    [Install]
    WantedBy=graphical.target
EOL

    # enable unit file & restart
    sudo systemctl daemon-reload
    sudo systemctl enable mount-cutting-edge-dropbox.service
    sudo systemctl start mount-cutting-edge-dropbox.service
}

# create and start timer to one-way sync (download) the cutting edge dropbox
function enable_sync_service() {
    # create download location
    DIR=~/cutting-edge-dropbox
    mkdir -p $DIR

    # create link on desktop
    link=~/Desktop/Cutting\ Edge\ Dropbox
    if [ -e "$link" ]; then
        rm "$link"
    fi
    ln -s $DIR "$link"

    # create service file
    sudo tee /lib/systemd/system/sync-cutting-edge-dropbox.service >> /dev/null <<EOL
[Unit]
Description=Cutting Edge Dropbox Sync task
Requires=network-online.target

[Service]
Type=oneshot
ExecStart=sh -c 'rclone --transfers 2 sync cutting-edge-dropbox: $DIR'
User=pi
Group=pi
EOL

    # create timer file
    sudo tee /lib/systemd/system/sync-cutting-edge-dropbox.timer >> /dev/null <<EOL
[Unit]
Description=Timer for the Cutting Edge Dropbox sync

[Timer]
OnBootSec=1min
OnUnitActiveSec=5min

[Install]
WantedBy=timers.target
EOL

    # enable unit file & restart
    sudo systemctl daemon-reload
    sudo systemctl enable sync-cutting-edge-dropbox.service
    sudo systemctl enable sync-cutting-edge-dropbox.timer
    sudo systemctl start sync-cutting-edge-dropbox.timer
}
enable_sync_service

# install GUI sync tool

cp ./cutting-edge-sync-tool.py ~/.local/bin/
cat ./cutting-edge-sync-tool.desktop | sed "s|\$HOME|$HOME|g" > ~/.local/share/applications/cutting-edge-sync-tool.desktop

# install a better PDF viewer and set to default

sudo apt update && sudo apt install evince
xdg-mime default evince.desktop application/pdf

# continue installing GUI
# download and install appimagelauncher deb file

wget -O appimagelauncher.deb https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.2.0/appimagelauncher_2.2.0-travis995.0f91801.bionic_armhf.deb
sudo dpkg -i appimagelauncher.deb
rm appimagelauncher.deb

# configure AppImageLauncher

cat >> ~/.config/appimagelauncher.cfg <<EOL
[AppImageLauncher]
# destination = ~/Applications
# enable_daemon = true
EOL

# install RcloneBrowser AppImage

wget -O rclone-browser.AppImage https://github.com/kapitainsky/RcloneBrowser/releases/download/1.8.0/rclone-browser-1.8.0-a0b66c6-raspberrypi-armhf.AppImage
chmod +x rclone-browser.AppImage
ail-cli integrate rclone-browser.AppImage 

# all done

echo "
Install script complete. It's been a pleasure.
"
