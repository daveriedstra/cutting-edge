# Cutting Edge Raspberry Pi configuration script

This script will configure a raspberry pi to sync with a DropBox account and install some other quality-of-life utilities.

## To run:

Open a terminal and enter this line:

```
curl https://gitlab.com/daveriedstra/cutting-edge/-/raw/master/configure.sh | bash
```

A web browser will open during the install process to ask for your DropBox login details, but otherwise everything is automated.

The Raspberry Pi will now one-way sync (download) the DropBox account data every 5 minutes while it's on and connected to the internet.

## To change dropbox account or update password

Open a terminal and enter this line:

```
rclone config reconnect cutting-edge-dropbox:
```

Press `enter` for all prompts until the web browser opens to ask for your DropBox login details.


## GUI

* Update / Change dropbox login
    * button that calls `rclone config reconnect` and skips through until OAuth
* Enable / Disable sync
    * just enables / disables systemctl timer
    * reflects current state (GUI must read timer unit file on load)
* Shows sync interval, sync dir, & sync usage
~~* Change sync interval
    * reads/writes timer unit file directly & refreshes systemctl~~
~~* Change sync dest folder
    * reads/writes to config file which is also read by sync service
* Change sync src folder
    * gui for browsing & selecting src...~~

Also needs 
* [x] gui should update when sync state changed (via checkbox)
* [x] (not installed state?)
* [x] .desktop file
* [x] icon?
* [x] update process (is this a git repo? self-updating?)
* [ ] documentation...
